var isMobile = false;
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
document.addEventListener('DOMContentLoaded', function(event) {
    if(document.getElementById('canv')) {
        initCanvas();
    }
    setCanvasClickListener();
    setCloseCanvasListener();
});


function initCanvas() {
    var vectorNumber = 0;
    var deviceWidth = window.innerWidth;
    var deviceHeight = window.innerHeight;
    const config = {
        inputSize: 625,
        outputSize: 2,
        activation: 'sigmoid'
    };
    function DCanvas(el)
    {
        const ctx = el.getContext('2d');
        var pixel;
        if(isMobile) {
            canv.width = 350;
            canv.height = 350;
            pixel = 14;

        } else {
            canv.width = 500;
            canv.height = 500;
            pixel = 20;
            document.querySelector('.buttons').style.left = '39%';
            document.querySelector('.buttons').style.marginTop = '17%';
            canv.style.top = '10%';
        }
        let is_mouse_down = false;
        this.drawLine = function(x1, y1, x2, y2, color = 'gray') {
            ctx.beginPath();
            ctx.strokeStyle = color;
            ctx.lineJoin = 'miter';
            ctx.lineWidth = 1;
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.stroke();
        };
        this.drawCell = function(x, y, w, h) {
            ctx.fillStyle = 'blue';
            ctx.strokeStyle = 'blue';
            ctx.lineJoin = 'miter';
            ctx.lineWidth = 1;
            ctx.rect(x, y, w, h);
            ctx.fill();
        };

        this.clear = function() {
            ctx.clearRect(0, 0, canv.width, canv.height);
        };

        this.drawGrid = function() {
            const w = canv.width;
            const h = canv.height;
            const p = w / pixel;
            const xStep = w / p;
            const yStep = h / p;
            for( let x = 0; x < w; x += xStep )
            {
                this.drawLine(x, 0, x, h);
            }
            for( let y = 0; y < h; y += yStep )
            {
                this.drawLine(0, y, w, y);
            }
        };

        this.calculate = function(draw = false) {
            const w = canv.width;
            const h = canv.height;
            const p = w / pixel;
            const xStep = w / p;
            const yStep = h / p;
            const vector = [];
            let __draw = [];
            for( let x = 0; x < w; x += xStep )
            {
                for( let y = 0; y < h; y += yStep )
                {
                    const data = ctx.getImageData(x, y, xStep, yStep);
                    let nonEmptyPixelsCount = 0;
                    for( i = 0; i < data.data.length; i += 10 )
                    {
                        const isEmpty = data.data[i] === 0;
                        if( !isEmpty )
                        {
                            nonEmptyPixelsCount += 1;
                        }
                    }
                    if( nonEmptyPixelsCount > 1 && draw )
                    {
                        __draw.push([x, y, xStep, yStep]);
                    }
                    vector.push(nonEmptyPixelsCount > 1 ? 1 : 0);
                }
            }
            if( draw )
            {
                this.clear();
                this.drawGrid();

                for( _d in __draw )
                {
                    this.drawCell( __draw[_d][0], __draw[_d][1], __draw[_d][2], __draw[_d][3] );
                }
            }

            return vector;
        };

        el.addEventListener('mousedown', function(e) {
            is_mouse_down = true;
            ctx.beginPath();
        });

        el.addEventListener('touchstart', function(e) {
            is_mouse_down = true;
            ctx.beginPath();
        });

        el.addEventListener('touchend', function(e) {
            is_mouse_down = false;
        });

        el.addEventListener('mouseup', function(e) {
            is_mouse_down = false;
        });

        el.addEventListener('mousemove', function(e) {
            if( is_mouse_down )
            {
                ctx.fillStyle = 'red';
                ctx.strokeStyle = 'red';
                ctx.lineWidth = pixel;

                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.stroke();

                ctx.beginPath();
                ctx.arc(e.offsetX, e.offsetY, pixel / 2, 0, Math.PI * 2);
                ctx.fill();

                ctx.beginPath();
                ctx.moveTo(e.offsetX, e.offsetY);
            }
        });

        el.addEventListener('touchmove', function(e) {
            e.preventDefault();
            var deltaWidth = (deviceWidth - e.target.width)/2,
                deltaHeight = (deviceHeight - e.target.height)/2;
            console.log(deltaWidth);
            console.log(deltaHeight);
            console.log(e.touches[0].clientX + ", " + e.touches[0].clientY);
            if( is_mouse_down )
            {
                ctx.fillStyle = 'pink';
                ctx.strokeStyle = 'pink';
                ctx.lineWidth = pixel;

                ctx.lineTo(e.touches[0].clientX, e.touches[0].clientY);
                //ctx.lineTo(e.touches[0].clientX - deltaWidth, e.touches[0].clientY - deltaHeight);
                ctx.stroke();

                ctx.beginPath();
                ctx.arc(e.touches[0].clientX, e.touches[0].clientY, pixel / 2, 0, Math.PI * 2);
                //ctx.arc(e.touches[0].clientX - deltaWidth, e.touches[0].clientY - deltaHeight, pixel / 2, 0, Math.PI * 2);
                ctx.fill();

                ctx.beginPath();
                //ctx.moveTo(e.touches[0].clientX - deltaWidth, e.touches[0].clientY - deltaHeight);
                ctx.moveTo(e.touches[0].clientX, e.touches[0].clientY);
            }
        })
    }

    let vector = [];
    let net = null;
    let train_data = [];

    const d = new DCanvas(document.getElementById('canv'));


    document.addEventListener('click', function(e) {
       if(e.target.id === "clear") {
           d.clear();
       }
       if(e.target.id === "store") {
           vector = d.calculate(true);
           setGraphicalPasswordVal(vector);
           if(document.getElementById('sign_up')) {
               storePassword();
           } else {
               if(document.getElementById('change_password')) {
                   changePassword();
               }
           }
       }
       if(e.target.id === "calc") {
           var response = getGraphicalPassword();
           setTimeout(function() {
               if(response.success) {
                   var password_hash = hash(document.getElementById('user_password').value);
                   calculateError(decrypt(response.data.graphical_password, password_hash));
               } else {
                   setTimeout(function() {
                       // Переделать так, чтобы в случае неправильной учетки в нейросеть отправлялась какая-то комбинация заведомо неправильная
                       $('.js-overlay-canvas').fadeOut();
                       $('.main').css('filter', 'none');
                       document.querySelector('.error-message').innerHTML = "Введены неверные данные";
                       setTimeout(function() {
                           document.querySelector('.error-message').innerHTML = "";
                       }, 2000);
                   }, 1000);
               }
               }, 1000);
       }
    });
    function randomBoolean() {
        var rand = 0 - 0.5 + Math.random() * (1 - 0 + 1)
        rand = Math.round(rand);
        return rand + 0;
    }
    function calculateError(userGraphicalPassword) {
        for(var i = 0; i < 10; i++) {
            var testVector = [];
            for(var j = 0; j < 625; j++) {
                testVector.push(randomBoolean());
            }
            train_data.push({
                input: testVector,
                output: { wrong: 1 }
            });
        }
        train_data.push({
            input: userGraphicalPassword,
            output: { correct: 1 }
        });
        net = new brain.NeuralNetwork(config);
        net.train(train_data, {
            learningRate: 0.3,
            iterations: 20,
            errorThresh: 0.0005,
            momentum: 0.4
        });

        //const result = brain.likely(d.calculate(), net);
        const result = net.run(d.calculate());
        if((result.correct/result.wrong > 2.5) || (result.correct > 0.91)) {
            sign_in(true);
        } else {
            sign_in(false);
        }
    }
}

function openCanvas() {
    $('.main').css('filter', 'blur(5px)');
    $('.js-overlay-canvas').fadeIn();
}
function setCloseCanvasListener() {
    $('.js-close-canvas').click(function() {
        $('.js-overlay-canvas').fadeOut();
        $('.main').css('filter', 'none');
    });
}

function setCanvasClickListener() {
    $(document).mouseup(function(e) {
        var popup = $('.js-overlay-canvas');
        if(e.target != popup[0] && e.target.id != 'clear' && e.target.id != 'store' && e.target.id != 'calc' && popup.has(e.target).length === 0) {
            $('.js-overlay-canvas').fadeOut();
            $('.main').css('filter', 'none');
        }
    });
}