function encrypt(password, key) {
    return CryptoJS.AES.encrypt(password, key).toString();
}

function hash(password) {
    return CryptoJS.SHA256(password).toString();
}

function decrypt(password, key) {
    return CryptoJS.AES.decrypt(password, key).toString(CryptoJS.enc.Utf8);
}