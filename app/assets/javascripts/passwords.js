function initResetForm() {
    var button = document.getElementById('change_password');
    button.addEventListener('click', function(ev) {
        ev.preventDefault();
        openCanvas();
    });
}

function changePassword() {
    var form = $('form[id=new_user]'),
        url = form.attr('action'),
        curFormData = form.serialize(),
        token = document.getElementsByTagName('meta')['csrf-token'].content,
        request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var curFormRequestData = JSON.parse(request.responseText);
                $('.js-overlay-canvas').fadeOut();
                $('.main').css('filter', 'none');
                if (curFormRequestData.success) {
                    document.querySelector('.message').textContent = curFormRequestData.message;
                    setTimeout(function () {
                        window.location.href = "/";
                    }, 2000);
                } else {
                    document.querySelector('.error-message').textContent = curFormRequestData.message;
                    setTimeout(function () {
                        document.querySelector('.error-message').innerHTML = "";
                    }, 3000);
                }
            } else {
                console.log("Ошибка на сервере");
            }
        }
    };
    request.open('POST', url + '?' + curFormData, true);
    request.setRequestHeader('X-CSRF-Token', token);
    request.send();
}