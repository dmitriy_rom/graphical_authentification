document.addEventListener('DOMContentLoaded', function(event) {
    if(document.getElementById('sign_up')) {
        initRegForm();
    }
    if(document.getElementById('sign_in')) {
        initSessionForm();
    }
    if(document.getElementById('change_password')) {
        initResetForm();
    }
    if(document.getElementById('comment')) {
        initFeedbackForm();
    }
});

function setGraphicalPasswordVal(vector) {
    var password = $('#user_password').val(),
        password_hash = hash(password),
        encrypted_graphical_password;
    encrypted_graphical_password = encrypt(vector.join(''), password_hash);
    document.getElementById('user_graphical_password').value = encrypted_graphical_password;
}

function initRegForm() {
    var button = document.getElementById('sign_up'),
        registrationForm = $('form[name=registration]');
    button.addEventListener('click', function(ev) {
        ev.preventDefault();
        sendTextPassword(registrationForm);
    });
}

function sendTextPassword(form) {
    var url = '/validate_user_data',
        curFormData = form.serialize(),
        token = document.getElementsByTagName('meta')['csrf-token'].content,
        request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var curFormRequestData = JSON.parse(request.responseText);
                if (curFormRequestData.success) {
                    openCanvas();
                } else {
                    document.querySelector('.error-message').innerHTML = curFormRequestData.message;
                    setTimeout(function() {
                        document.querySelector('.error-message').innerHTML = "";
                    }, 3000);
                }
            } else {
                console.log("Ошибка на сервере");
            }
        }
    };
    request.open('POST', url + '?' + curFormData, true);
    request.setRequestHeader('X-CSRF-Token', token);
    request.send();
}

function storePassword() {
    var form = $('form[name=registration]'),
        url = form.attr('action'),
        curFormData = form.serialize(),
        token = document.getElementsByTagName('meta')['csrf-token'].content,
        request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var curFormRequestData = JSON.parse(request.responseText);
                $('.js-overlay-canvas').fadeOut();
                $('.main').css('filter', 'none');
                if (curFormRequestData.success) {
                    document.querySelector('.message').textContent = "В течение нескольких минут" +
                        " Вы получите на почту инструкцию по подтверждению учетной записи"
                } else {
                    console.log("Произошла ошибка, повторите попытку позже");
                    document.querySelector('.message').textContent = "Произошла ошибка, повторите попытку позже"
                }
            } else {
                console.log("Ошибка на сервере");
            }
        }
    };
    request.open('POST', url + '?' + curFormData, true);
    request.setRequestHeader('X-CSRF-Token', token);
    request.send();
}

function initFeedbackForm() {
    document.forms[0].addEventListener('submit', function (ev) {
        if(document.getElementById('comment').value != '') {
            //document.querySelector('input[name=commit]').disabled = true;
            //document.querySelector('input[name=commit]').value = "Сообщение успешно отправлено";
            document.querySelector('input[name=commit]').style.display = 'none';
            document.querySelector('.delivery_message').innerHTML = "Сообщение успешно отправлено";
        }
    });
}