function initSessionForm() {
    var button = document.getElementById('sign_in');
    button.addEventListener('click', function(ev) {
        ev.preventDefault();
        openCanvas();
    });
}

function getUserGraphicalPassword() {
    var sessionForm = $('form[name=session]'),
        url = '/getUserGraphicalPassword',
        curFormData = sessionForm.serialize(),
        request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var curFormRequestData = JSON.parse(request.responseText);
                if (curFormRequestData.success) {
                    calculateError(curFormRequestData.data.graphical_password);
                } else {
                    calculateError('wrong password');
                }
            } else {
                console.log("Ошибка на стороне сервера");
            }
        }
    };
    request.open('GET', url + '?' + curFormData, false);
    request.send();
}

function getGraphicalPassword() {
    var sessionForm = $('form[name=session]'),
        url = '/getUserGraphicalPassword',
        curFormData = sessionForm.serialize(),
        result;
    $.ajax({
        url:url,
        async: false,
        data: curFormData ,
        success:function(data) {
            result = data;
        }
    });
    return result;
}

function sign_in(correctGraphicalPassword) {
    var sessionForm = $('form[name=session]'),
        url = '/create_session',
        token = document.getElementsByTagName('meta')['csrf-token'].content,
        curFormData = sessionForm.serialize(),
        request = new XMLHttpRequest();
    url = url + '?' + curFormData + '&correct_password=' + encodeURIComponent(correctGraphicalPassword);
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                var curFormRequestData = JSON.parse(request.responseText);
                if (curFormRequestData.success) {
                    document.querySelector('.message').innerHTML = "Вход в систему выполнен";
                    setTimeout(function () {
                        window.location.href = "/";
                    }, 2000);
                } else {
                    document.querySelector('.error-message').innerHTML = curFormRequestData.message;
                    setTimeout(function () {
                        document.querySelector('.error-message').innerHTML = "";
                    }, 3000);
                }
                $('.js-overlay-canvas').fadeOut();
                $('.main').css('filter', 'none');
            }
        }
    };
    request.open('POST', url, true);
    request.setRequestHeader('X-CSRF-Token', token);
    request.send();
}