# frozen_string_literal: true

class Users::PasswordsController < Devise::PasswordsController
  # GET /resource/password/new
  # def new
  #   super
  # end

  # POST /resource/password
  def create
    user = User.find_by(reset_password_token: params[:user][:reset_password_token])
    if user.present?
      if params[:user][:password].length >= 6
        if params[:user][:password] == params[:user][:password_confirmation]
          user.update(password: params[:user][:password], graphical_password: params[:user][:graphical_password])
          sign_in(user, scope: :user)
          return render :json => { :success => true, :message => "Ваш пароль успешно изменен" }
        else
          return render :json => { :success => false, :message => "Подтверждение пароля не совпадает с паролем." }
        end
      else
        return render :json => { :success => false, :message => "Пароль должен быть не менее 6 символов." }
      end
    else
      return render :json => { :success => false, :message => "Извините, произошла ошибка. Повторите попытку позже." }
    end
  end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  # def update
  #   super
  # end

  # protected

  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   super(resource_name)
  # end
end
