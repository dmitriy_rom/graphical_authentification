# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  require 'uri'
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def validate_user_data
    if params[:user][:email].present? && params[:user][:password].present? && params[:user][:password_confirmation].present?
      if User.find_by(email: params[:user][:email]).present?
        return render :json => { :success => false, :message => "Пользователь #{params[:user][:email]} уже зарегистрирован" }
      else
        if params[:user][:email].match(URI::MailTo::EMAIL_REGEXP).present?
          if params[:user][:password] == params[:user][:password_confirmation]
            if params[:user][:password].length >= 6
              return render :json => { :success => true }
            else
              return render :json => { :success => false, :message => "Пароль должен содержать не менее 6 символов" }
            end
          else
            return render :json => { :success => false, :message => "Неверно введено подтверждение пароля" }
          end
        else
          return render :json => { :success => false, :message => "Произошла ошибка. Проверьте правильность введенного email" }
        end
      end
    else
      return render :json => { :success => false, :message => "Пожалуйста, заполните форму регистрации" }
    end
  end

  def create
    puts params[:user][:graphical_password]
    user = User.new(email: params[:user][:email],
                    password: params[:user][:password],
                    graphical_password: params[:user][:graphical_password])
    if user.valid?
      user.save
      return render :json => { :success => true }
    else
      return render :json => { :success => false }
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
