# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    super
  end

  def get_user_graphical_password
    puts "Есть заход в метод кста"
    puts params[:user][:email]
    puts params[:user][:password]
    user = User.find_by(email: params[:user][:email])
    if user.present?
      if user.valid_password?(params[:user][:password])
        return render :json => { :success => true, :data => { :graphical_password => user.graphical_password } }
      else
        return render :json => { :success => false, :message => "Введены неверные данные" }
      end
    else
      return render :json => { :success => false, :message => "Введены неверные данные" }
    end
  end

  # POST /resource/sign_in
  def create
    puts "Контроллер сессии есл чо"
    puts params[:correct_password]
    if params[:correct_password] == "true"
      current_user = User.find_by(email: params[:user][:email])
      if current_user.present?
        if current_user.confirmed?
          if current_user.valid_password?(params[:user][:password])
            puts "Правильный пароль"
            sign_in(current_user, scope: :current_user)
            return render :json => { :success => true, :message => "Все окей" }
          end
        else
          return render :json => { :success => false, :message => "Пожалуйста, подтвердите свою учетную запись" }
        end
      else
        puts "Неправильный пароль"
        return render :json => { :success => false, :message => "Введены неверные данные" }
      end
    else
      puts "Неправильный пароль"
      return render :json => { :success => false, :message => "Введены неверные данные" }
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
