class ApplicationMailer < ActionMailer::Base
  default from: 'temprobot@proektmarketing.ru'
  layout 'mailer'
end
