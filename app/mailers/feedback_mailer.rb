class FeedbackMailer < ApplicationMailer

  def feedback_message(message)
    email = 'romanenko.dmitriy.97@yandex.ru'
    subject = "Вам оставили комментарий с сайта о дипломной работе"
    @message = message
    mail(to: email, subject: subject) do |format|
      format.html
    end
  end

end