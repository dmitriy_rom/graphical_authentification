Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions', passwods: 'users/passwords' }
  devise_scope :user do
    post '/validate_user_data', to: 'users/registrations#validate_user_data', as: :validate_user_data
    get '/getUserGraphicalPassword', to: 'users/sessions#get_user_graphical_password', as: :get_user_graphical_password
    post '/create_session', to: 'users/sessions#create', as: :create_user_session
    post '/update_password', to: 'users/passwords#create', as: :update_password
    post '/feedback', to: 'home#feedback', as: :feedback
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
end
